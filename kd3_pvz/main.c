/*
 *
 *  - Priimtų du argumentus - katalogą ir sveiką skaičių N.i
 *  - Nurodytame kataloge ieškotų paprasto failo (tikrinant ar jis paprastas),
 *  kuris būtų didesnis už 4KB
 *  - Radus pirmą tokį failą
 *	- a. pabandytų "primapinti" į proceso adresų erdvę. Jei pavyko:
 *	- b. praplėsti failą failo pabaigoje prirašant 10 baitų informacijos iš
 *	failo pradžios (įvertinkite, kad po “primapinimo” failo dydžio keisti
 *	negalima, tad jį reiks pasididinti dar prieš prijungiant failą į
 *	proceso erdvę)
 *	- c. iškviestų šio failo N-ojo baito reikšmę (iš primapinto regiono) į
 *	ekraną (kaip sveiką skaičių). Pastaba. N nurodomas antru komandinės
 *	eilutės argumentu (N prasmė kaip C masyvo indekso, t.y. N=0 -> pirmas
 *	baitas); Argumento vertimui į skaičių, galima naudoti atoi();
 *  - Programa turi apdoroti galimas klaidas.
 */
#define _XOPEN_SOURCE 500 /* SUSv1 support */
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>
#include <ftw.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

#define C_BUF_SIZE 1000
#define DEBUG 0

static int is_dir(char *arg)
{
	struct stat info;

	stat(arg, &info);

	if (!S_ISDIR(info.st_mode)) {
		perror("first argument not a directory");
		return 0;
	}

	return 1;
}

/*
 * works partially: ./main . 34sd
 */
static int is_digit(char *arg)
{
	long int temp;

	temp = strtol(arg, NULL, 10);
	if (temp == LONG_MAX || temp == LONG_MIN || temp < 0) {
		perror("first argument not a directory");
		exit(1);
	}

	return (int)temp;
}

static int open_file(const char *name, int flags, mode_t mode)
{
	int fd;

	fd = open(name, flags, mode);
	if (fd == -1) {
		perror("Unable to properly open file");
		exit(1);
	}

	return fd;
}

static int display_info(const char *fpath, const struct stat *sb,
			int tflag, struct FTW *ftwbuf)
{
	/* skip if not regular file or smaller then 4MB */
	if (tflag != FTW_F || sb->st_size <= 4096)
		return 0;

#if DEBUG
	printf("%s\n", &fpath[ftwbuf->base]);
#endif

	return open_file(fpath, O_RDWR, 0640);
}

static int file_read(int fd, char* buf)
{
	int temp;

	lseek(fd, 0, SEEK_SET);
	temp = (int)read(fd, buf, 10);
	if (temp == -1){
		perror("Unable to read from file");
		exit(1);
	}

	return temp;
}

static int file_write(int fd, char* buf, int size)
{
	int temp;

	lseek(fd, size, SEEK_SET);
	temp = write(fd, buf, 10);
	if (temp == -1){
		perror("profesorius bibys\n");
		exit(1);
	}

	return temp;
}

static void *io_mmap(int fd, int size)
{
	void *temp = NULL;

	temp = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (temp == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	return temp;
}

static int *io_unnmap(void *a, int size)
{
	if (munmap(a, size)) {
		puts("munmap failed");
		exit(1);
	}

	return 0;
}

int main(int argc, char *argv[])
{
	char buf[C_BUF_SIZE];
	struct stat statbuf;
	int n;
	int ftw = -1;
	void *map;

	if (argc != 3 || !is_dir(argv[1]) || !(n = is_digit(argv[2]))) {
		fprintf(stderr, "Usage: %s <folder> <offset>\n", argv[0]);
		exit(1);

		return 1;
	}

	memset(&buf, 0, sizeof(buf));
	/* search for file in dir */
	ftw = nftw(argv[1], display_info, 20, FTW_PHYS);
	if (ftw == -1) {
		fprintf(stderr, "File not found\n");
		exit(1);
	}

	fstat(ftw, &statbuf);
	file_read(ftw, buf);
	file_write(ftw, buf, statbuf.st_size);

	map = io_mmap(ftw, C_BUF_SIZE);
	io_unnmap(map, C_BUF_SIZE);
	return 0;
}

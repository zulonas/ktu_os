/*
 * Sukurkite programą loginas_pipe02.c, kuri paleistų du procesus ir iš tėvo
 * vaikui persiųstų komandinės eilutės argumentu nurodomo failo turinį
 * (panaudojant pipe()).
 *
 */
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define C_BUF_SIZE 200

int main(int argc, char *argv[])
{
	FILE *file;
	int pipefd[2];
	char buf[C_BUF_SIZE];
	pid_t cpid;
	char temp;


	if (argc != 2) {
		fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		exit(1);
	}

	if (pipe(pipefd) == -1) {
		perror("pipe");
		exit(1);
	}
	memset(buf, 0, sizeof(buf));

	switch (cpid = fork()) {
	case -1:
		perror("fork");
		exit(1);
		break;
	case 0:
		/* child */
		close(pipefd[1]); /* close unused write end */
		while (read(pipefd[0], &temp, 1) > 0)
			write(STDOUT_FILENO, &temp, 1);

		close(pipefd[0]);
		return 0;
		break;
	default:
		/* parent */
		close(pipefd[0]); /* close unused read end*/
		file = fopen(argv[1], "r");
		if (!file) {
			perror("read");
			exit(1);
		}
		fread(buf, sizeof(buf), 1, file);
		fclose(file);
		write(pipefd[1], buf, sizeof(buf));
		close(pipefd[1]);
		wait(NULL); /* wait for child */
		return 0;
	}

	return 0;
}

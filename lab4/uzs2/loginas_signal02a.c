/*
 * Modifikuokite programą (source:posix|inglagz_signal02.c) pavadindami
 * loginas_signal02a.c taip, kad:
 *	 - vaiko procesas, gavęs signalą SIGUSR1 iš tėvo proceso įvykdo komandą
 *	 who ir siunčia tėvo procesui signalą SIGUSR2.
 *	 - tėvo procesas, gavęs iš vaiko proceso signalą, vaiko procesą
 *	 "nužudo", palaukia 5 sekundes, išspausdina pranešimą apie darbo
 *	 pabaigą ir baigiasi.
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>

static int received_sig1 = 0;
static int received_sig2 = 0;

int il_child(void)
{
	sleep(1);
	printf("\tchild: my ID = %d\n", getpid());
	while (received_sig1 != 1)
		;

	printf("\tchild: Received signal from parent!\n");
	printf("\n");
	system("who");
	printf("\n");
	kill(getppid(), SIGUSR2);
	sleep(1);
	printf("\tchild: I'm exiting\n" );

	return 0;
}

int il_parent(pid_t pid)
{
	printf("parent: my ID = %d\n", getpid());
	printf("parent: my child's ID = %d\n", pid);

	sleep(3);

	kill(pid, SIGUSR1);
	printf("parent: Signal was sent\n");
	while (received_sig2 != 1)
		;

	printf("parent: Received signal from child!\n");
	kill(pid, SIGKILL);
	sleep(5);
	printf("parent: exiting.\n");

	return 0;
}

void il_catch_USR1(int snum)
{
	received_sig1 = 1;
}

void il_catch_USR2(int snum)
{
	received_sig2 = 1;
}

int main(int argc, char **arg)
{
	pid_t pid;

	signal(SIGUSR1, il_catch_USR1);
	signal(SIGUSR2, il_catch_USR2);
	switch (pid = fork()) {
	case 0:                                         /* fork() grazina 0 vaiko procesui */
		il_child();
		break;
	default:                                        /* fork() grazina vaiko PID tevo procesui */
		il_parent(pid);
		break;
	case -1:                                        /* fork() nepavyko */
		perror("fork");
		exit(1);
	}

	return 0;
}

/*
 * inglagz_signal01: tevo procesas sukuria vaiko procesa
 * ir laukia vaiko proceso darbo pabaigos signalo
 * Jo sulaukes darba baigia pats
 * Vaiko procesas isspausdina pranesima ir palaukes 3s baigia darba
 *
 * Modifikuokite programą (source:posix|inglagz_signal01.c) pavadindami ją
 * loginas_signal01a.c taip, kad
 *	- vaiko procesas prieš baigdamas savo darbą išsiųstų tėvo procesui
 *	alarmo signalą SIGALRM
 *	- šis gavęs šį signalą jį apdorotų išvesdamas pranešimą, kad signalą
 *	gavo (nurodant jo numerį).
 *
 */
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>

void il_child(void)
{
	printf("        child: I'm the child\n");
	sleep(3);
	kill(getppid(), SIGALRM);
	printf("        child: I'm exiting\n");
	exit(123);
}

void il_parent(int pid)
{
	printf("parent: I'm the parent (PID=%d)\n", getpid());
	sleep(10);
	printf("parent: exiting\n");
}

void il_catch_CHLD(int snum)
{
	int pid;
	int status;

	pid = wait(&status);
	printf("parent: child process (PID=%d) exited with value %d\n", pid,
	       WEXITSTATUS(status));
}

int main(int argc, char **argv)
{
	pid_t pid;

	signal(SIGALRM, il_catch_CHLD); /* set default signal handler */
	switch (pid = fork()) {
	case 0:
		il_child();
		break;
	default:
		il_parent(pid);
		break;
	case -1:
		perror("fork");
		exit(1);
	}

	return 0;
}

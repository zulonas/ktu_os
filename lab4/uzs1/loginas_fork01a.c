/*
 * Išsiaiškinkite (sukurkite tai demonstruojančią programą loginas_fork01.c,
 * nusipaišykite sukurtų procesų medį (kad matytųsi kas ką paleido)), kiek
 * procesų sukurs programa, kurioje vykdomas fragmentas:
 * fork();
 * fork();
 *
 * Pataisykite loginas_fork01.c (nusikopijuokite į loginas_fork01a.c) taip, kad
 * būtų sukuriami 3 procesai: tėvas -> vaikas -> anūkas.
 *
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	pid_t pid;

	printf("hello world from -  %d\n", getpid());

	pid = fork();
	if (!pid) {
		printf("\thello world from -  %d child\n", getpid());
		pid = fork();
	}
	if (!pid)
		printf("\t\thello world from -  %d grandchild\n", getpid());

	return 0;
}

/*
 * Išsiaiškinkite (sukurkite tai demonstruojančią programą loginas_fork01.c,
 * nusipaišykite sukurtų procesų medį (kad matytųsi kas ką paleido)), kiek
 * procesų sukurs programa, kurioje vykdomas fragmentas:
 * fork();
 * fork();
 *
 * Pataisykite loginas_fork01.c (nusikopijuokite į loginas_fork01a.c) taip, kad
 * būtų sukuriami 3 procesai: tėvas ir 2 jo vaikai.
 *
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	pid_t initial = getpid();
	printf("hello world from -  %d\n", initial);

	if (!fork())
		printf("\thello world from -  %d child\n", getpid());
	else
		if (!fork())
			printf("\thello world from -  %d child\n", getpid());

	return 0;
}

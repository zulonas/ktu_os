/*
 * Sukurkite programą loginas_exec01.c, kuri:
 * - priimtų vieną argumentą – sveiką skaičių;
 * - atspausdintų savo PID, PPID ir gautą argumentą;
 * - jei argumento reikšmė >0 paleistų save pačią su exec(), bet nurodydama
 * vienetu mažesnę argumento reikšmę.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int value;
	char buf[10];

	if (argc != 2 || !(value = atoi(argv[1]))) {
		fprintf(stderr, "Usage: %s <number>\n", argv[0]);
		return 0;
	}

	printf("PID = %d\nPPID = %d\nargument = %d\n\n", getpid(), getppid(),
	       value);


	snprintf(buf, sizeof(buf), "%d", value - 1);
	execl(argv[0], argv[0], buf, (char *) NULL);

	return 0;
}

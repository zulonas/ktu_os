/*
 * Išsiaiškinkite, kaip procesų sąraše matomas "zombis", t.y. pasibaigęs vaiko
 * procesas, kol tėvas su wait() dar nenuskaitė vaiko pabaigos būsenos?
 * Sukurkite tai demonstruojančią programą loginas_fork03.c. Procesų sąrašui
 * gauti galite naudoti ps komandą su tinkamais argumentais (jos iškvietimui
 * galite naudoti system() arba exec() savo nuožiūra).
 *
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <libgen.h>

int main(int argc, char *argv[])
{
	char cmd[50];
	pid_t child_pid;
	int status;

	printf("Parrent PID=%d\n", getpid());

	child_pid = fork();
	if (!child_pid) {
		printf("Child (PID=%d) exiting\n", getpid());
		exit(0);
	}

	sleep(1);
	printf("\n");
	snprintf(cmd, sizeof(cmd), "ps %d", child_pid);
	system(cmd);

	child_pid = wait(&status);
	if (WIFEXITED(status))
		printf("\n[%d]\tProcess %d exited with status %d.\n", getpid(),
		       child_pid, WEXITSTATUS(status));

	return 0;
}

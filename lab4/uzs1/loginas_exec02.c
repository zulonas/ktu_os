/*
 * sukurkite programą loginas_exec02.c, kuri darbiniame kataloge sukurtų sh
 * skriptą ir jį paleistų.
 */
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#define C_FILE "script.sh"

int main()
{
	FILE *fp;
	char buf[] = "#!/bin/bash\nls";

	fp = fopen(C_FILE, "w+");

	fwrite(buf, 1, sizeof(buf), fp);
	fclose(fp);

	chmod(C_FILE, S_IRUSR | S_IWUSR | S_IXUSR);

	if (execl(C_FILE, C_FILE, (char *)NULL) == -1)
		printf("%s\n", strerror(errno));

	return 0;
}

/*
 * Išsiaiškinkite, kas tapo proceso, kurio tėvas pasibaigė, tėvu. Sukurkite tai
 * demonstruojančią programą loginas_fork02.c.
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
	char buf[25];
	pid_t initial = getpid();

	sprintf(buf, "pstree -sp %d", initial);
	system(buf);

	if (fork())
		exit(0);

	sprintf(buf, "pstree -sp %d", getpid());
	system(buf);

	return 0;
}

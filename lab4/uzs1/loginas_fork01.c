/*
 * Išsiaiškinkite (sukurkite tai demonstruojančią programą loginas_fork01.c,
 * nusipaišykite sukurtų procesų medį (kad matytųsi kas ką paleido)), kiek
 * procesų sukurs programa, kurioje vykdomas fragmentas:
 * fork();
 * fork();
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	printf("hello world from -  %d\n", getpid());

	fork();
	printf("\thello world from -  %d\n", getpid());
	fork();
	printf("\t\thello world from -  %d\n", getpid());

	return 0;
}

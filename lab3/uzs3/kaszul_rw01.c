/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_rq01.c
 *
 * Sukurkite programą loginas_rw01.c, kuri:
 *	- atidarytų komandinėje eilutėje nurodytą failą tik skaitymui su
 *	open();
 *	- atidarytų kitą komandinėje eilutėje nurodytą failą tik rašymui
 *	(sukurtų, jei nėra, išvalytų turinį jei jau yra);
 *	- nukopijuotų iš skaitomo failo į rašomą komandinėje eilutėje nurodytą
 *	baitų skaičių (jei tiek baitų nėra – tiek kiek yra, t. y. visą failą)
 *	naudojant read() ir write();
 *	- uždarytų abu failus su close().
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define C_BUFSIZE 1000

int open_file(char *name, int flags)
{
	int fd;

	fd = open(name, flags);
	if (fd == -1) {
		fprintf(stderr, "Unable to open %s corretly: %s\n", name,
			strerror(errno));
		exit(1);
	}

	return fd;
}

int main(int argc, char *argv[])
{
	char buf[C_BUFSIZE];
	int file1;
	int file2;
	size_t size;
	size_t bytes_read;
	size_t bytes_write;

	memset(&buf, 0, sizeof(buf));
	if (argc != 4) {
		fprintf(stderr, "Usage: %s <file> <file> <size>\n", argv[0]);
		exit(1);
	}

	size = (size_t) atoi(argv[3]);
	file1 = open_file(argv[1], O_RDONLY);
	file2 = open_file(argv[2], O_WRONLY | O_CREAT);

	bytes_read = read(file1, buf, sizeof(buf));
	if (bytes_read < size)
		size = (size_t) bytes_read;
	bytes_write = write(file2, buf, size);

	close(file2);
	close(file1);
	return 0;
}


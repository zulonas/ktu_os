/* Kasparas Zulonas IFIN-8/3 kaszul */
/* Failas: kaszul_aio02.c */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <aio.h>

#define C_BUFFLEN 1048576
#define C_FILE "/dev/urandom"

int kp_test_open(const char *name)
{
	int dskr;

	dskr = open(name, O_RDONLY);
	if (dskr == -1) {
		perror(name);
		exit(1);
	}

	return dskr;
}

int kp_test_close(int fd)
{
	int rv;

	rv = close(fd);
	if (rv == -1)
		perror ("close() failed");
	printf("closed");

	return rv;
}

int kp_test_aio_read_start(const int fd, struct aiocb *aiorp, void *buf, const
			    int count, const int offset)
{
	int rv = 0;

	memset((void *)aiorp, 0, sizeof(struct aiocb));
	aiorp->aio_fildes = fd;
	aiorp->aio_buf = buf;
	aiorp->aio_nbytes = count;
	aiorp->aio_offset = offset;
	rv = aio_read(aiorp);
	if (rv) {
		perror( "aio_read failed" );
		abort();
	}

	return rv;
}

int kp_test_dummy(const void *data, int n)
{
	int i, cnt = 0;

	for (i = 0; i < n; i++)
		if (((char*)data)[i] == '\0')
			cnt++;

	printf("Number of '0' in data: %d\n", cnt);

	return 0;
}

int kp_test_aio_read_waitcomplete(struct aiocb *aiorp)
{
	const struct aiocb *aioptr[1];
	int rv;

	aioptr[0] = aiorp;
	rv = aio_suspend(aioptr, 1, NULL);
	if (rv == -1) {
		perror( "aio_suspend failed" );
		abort();
	}
	rv = aio_return(aiorp);
	printf("AIO complete, %d bytes read.\n", rv);

	return 0;
}

int main(int argc, char *argv[])
{
	int fd;
	struct aiocb aior;
	char buffer[C_BUFFLEN];
	int offset = 0;
	int i;

	fd = kp_test_open(C_FILE);
	while (i < 10) {
		kp_test_aio_read_start(fd, &aior, buffer, sizeof(buffer),
				       offset);
		offset += sizeof(struct aiocb);
		kp_test_dummy(buffer, sizeof(buffer));
		kp_test_aio_read_waitcomplete(&aior);
		i++;
	}

	kp_test_close(fd);
	kp_test_dummy(buffer, sizeof(buffer));

	return 0;
}

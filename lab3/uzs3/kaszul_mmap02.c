/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_mmap02.c
 *
 * Sukurkite programa loginas_mmap02.c, kuri nukopijuotų failus naudojant
 * mmap() (kad būtų paprasčiau laikykime, kad failų dydžiai iki 100MB, t. y.
 * abu telpa į 32bit proceso erdvę):
 *	- atidarytų ir prijungtų 2 programos argumentais nurodytus failus su
 *	mmap() (vieną iš jų tik skaitymui, tik skaitomo failo dydį galite
 *	sužinoti su fstat() funkcija)
 *	- nukopijuotų vieno failo turinį į kitą (su memcpy() ar paprastu ciklu)
 *      - atjungtų abu failus
 *      - uždarytų abu deskriptorius
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <string.h>

#define SIZE 1048576

int kp_test_openw(const char *name)
{
	int dskr;

	dskr = open(name, O_RDWR | O_CREAT, 0640);
	if (dskr == -1) {
		perror(name);
		exit(1);
	}

	printf("dskr = %d\n", dskr);
	return dskr;
}

int kp_test_close(int fd)
{
	int rv;

	rv = close(fd);
	if (rv)
		perror("close() failed");
	else
		puts("closed");

	return rv;
}

void* kp_test_mmapw_read(int d, int size)
{
	void *a = NULL;

	lseek(d, size - 1, SEEK_SET);
	write(d, &d, 1);        /* iraso bile ka i failo gala */
	a = mmap(NULL, size, PROT_READ, MAP_SHARED, d, 0);
	if (a == MAP_FAILED) {
		perror("mmap failed");
		abort();
	}

	return a;
}

void *kp_test_mmapw(int d, int size)
{
	void *a = NULL;

	lseek(d, size - 1, SEEK_SET);
	write(d, &d, 1);        /* iraso bile ka i failo gala */
	a = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, d, 0);
	if (a == MAP_FAILED) {
		perror("mmap failed");
		abort();
	}

	return a;
}

int kp_test_munamp(void *a, int size)
{
	int rv;

	rv = munmap(a, size);
	if (rv) {
		puts("munmap failed");
		abort();
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int fd1;
	int fd2;
	struct stat st;
	void *a1;
	void *a2;

	if (argc != 3) {
		printf("Naudojimas: %s <failas> <failas>\n", argv[0]);
		exit(1);
	}

	fd1 = kp_test_openw(argv[1]);
	fd2 = kp_test_openw(argv[2]);
	a1 = kp_test_mmapw_read(fd1, SIZE);
	a2 = kp_test_mmapw(fd2, SIZE);
	fstat(fd1, &st);

	memcpy(a2, a1, st.st_size);

	kp_test_munamp(a1, SIZE);
	kp_test_munamp(a2, SIZE);
	kp_test_close(fd1);
	kp_test_close(fd2);

	return 0;
}

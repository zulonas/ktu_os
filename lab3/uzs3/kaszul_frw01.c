/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_rq01.c
 *
 * Sukurkite programą loginas_rw01.c, kuri:
 *	- atidarytų komandinėje eilutėje nurodytą failą tik skaitymui su
 *	open();
 *	- atidarytų kitą komandinėje eilutėje nurodytą failą tik rašymui
 *	(sukurtų, jei nėra, išvalytų turinį jei jau yra);
 *	- nukopijuotų iš skaitomo failo į rašomą komandinėje eilutėje nurodytą
 *	baitų skaičių (jei tiek baitų nėra – tiek kiek yra, t. y. visą failą)
 *	naudojant read() ir write();
 *	- uždarytų abu failus su close().
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define C_BUFSIZE 1000

FILE *open_file(char *name, char *flags)
{
	FILE *file;

	file = fopen(name, flags);
	if (!file) {
		fprintf(stderr, "Unable to open %s corretly: %s\n", name,
			strerror(errno));
		exit(1);
	}

	return file;
}

int main(int argc, char *argv[])
{
	char buf[C_BUFSIZE];
	FILE *file1;
	FILE *file2;
	size_t size;
	size_t bytes_read;

	memset(&buf, 0, sizeof(buf));
	if (argc != 4) {
		fprintf(stderr, "Usage: %s <file> <file> <size>\n", argv[0]);
		exit(1);
	}

	size = (size_t) atoi(argv[3]);
	file1 = open_file(argv[1], "r");
	file2 = open_file(argv[2], "w");

	if (sizeof(buf) < size)
		size = sizeof(buf);
	bytes_read = fread(buf, 1, sizeof(buf), file1);
	if (bytes_read < size)
		size = (size_t) bytes_read;
	fwrite(buf, 1, size, file2);

	fclose(file2);
	fclose(file1);
	return 0;
}


/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_seek01.c
 *
 * Sukurkite programą loginas_seek01.c, kuri:
 *	- sukurtų failą (su open() ar creat());
 *	- nueitų į 1MB gilyn į failą su lseek();
 *	- įrašytų 1 baitą;
 *	- uždarytų failą su close().
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	int file;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	file = creat(argv[1], mode);
	if (file == -1) {
		fprintf(stderr, "Error occured while creting %s file: %s\n",
			argv[1], strerror(errno));
		exit(1);
	}
	/* 1024b * 1024 = 1MB*/
	lseek(file, 1024 * 1024, SEEK_SET);

	write(file, "x", 1);

	close(file);
	return 0;
}


/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_cpulimit01.c
 *
 * Sukurkite programą loginas_cpulimit01.c, kuri nustatytu CPU limitą 1s
 * (RLIMIT_CPU) ir patikrinkite, ar limitas suveikia (nustatę limitą užsukite
 * amžiną ciką su skaitliuku).
 * - Suveikus limitui programa turėtų mest core.
 * - Kiek iteracijų padarė amžinas ciklas: ? (galite tai sužinoti iš core failo;
 * jei įtariate, kad skaitliukas persipildė – pabandykite perkompiliuoti į
 * - 64bit su kompiliatoriaus parametru -m64 arba sumažinti CPU laiko limitą).
 * - Pataisykite, kad programa nemestų core (nustatykite RLIMIT_CORE limitą į 0)
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/resource.h>

int mb_change_filelimit(int nslimit, int nhlimit)
{
	struct rlimit rl;
	struct rlimit rlc;

	getrlimit(RLIMIT_CPU, &rl);
	printf("RLIMIT_CPU %ld (soft) %ld (hard)\n", rl.rlim_cur, rl.rlim_max);

	rl.rlim_cur = nslimit;
	rl.rlim_max = nhlimit;
	rlc.rlim_cur = 0;
	setrlimit(RLIMIT_CPU, &rl);
	setrlimit(RLIMIT_CORE, &rlc);
	getrlimit(RLIMIT_CPU, &rl);
	getrlimit(RLIMIT_CORE, &rlc);

	printf("RLIMIT_CPU %ld (soft) %ld (hard)\n", rl.rlim_cur, rl.rlim_max);
	printf("RLIMIT_CORE %ld\n", rlc.rlim_cur);

	return 0;
}

int mb_test_filelimit(const char *name)
{
	int n, cnt;
	cnt = 0;
	int i = 1;

	while (i != 0)
		cnt++;
	for (n = 0; -1 != open(name, O_RDONLY); n++)
		;
	printf("Counteris %d\n", cnt);
	printf("Can open %d files\n", n);

	return 0;
}

int main(int argc, char *argv[])
{
	mb_change_filelimit(5, 10);
	mb_test_filelimit(argv[0]);

	return 0;
}

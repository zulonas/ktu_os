/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_nftw02.c
 *
 * Sukurkite ir išbandykite programą loginas_nftw02.c, kuri su nftw()
 * išvaikščiotų Jūsų namų katalogą ir atspausdintų visų jame esančių failų
 * pavadinimus (t. y. pradėtų paiešką nuo Jūsų namų katalogo ir naudotų
 * FTW_PHYS, kad neišeitų iš jo radus simbolinę nuorodą).
 */
#define _XOPEN_SOURCE 500 /* SUSv1 support */
#include <stdio.h>
#include <ftw.h>
#include <stdlib.h>
#include <sys/stat.h>

static int
display_info(const char *fpath, const struct stat *sb,
	     int tflag, struct FTW *ftwbuf)
{
	printf("%s\n", &fpath[ftwbuf->base]);

	return 0;
}

int main(int argc, char *argv[])
{
	int ftw;
	char *home_dir;

	if ((home_dir = (getenv("HOME"))) == NULL) {
		perror("Unable to determinate home dir");
		exit(1);
	}

	ftw = nftw(home_dir, display_info, 20, FTW_PHYS);
	if (ftw) {
		perror("ntfw error");
		exit(1);
	}

	return 0;
}


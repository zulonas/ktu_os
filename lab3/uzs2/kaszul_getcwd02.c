/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_getcwd02.c
 *
 * Sukurkite C programą loginas_getcwd02.c (loginas pakeiskite į savo loginą),
 * kuri:
 *	1. gautų ir atspausdintų einamo katalogo vardą su getcwd() (ir kviečiant
 *	getcwd() naudotų pathconf(".",_PC_PATH_MAX) grąžinamą reikšmę)
 *	2. atidarytų einamą katalogą su open(), įsimintų ir atspausdintų jo
 *	deskriptorių
 *	3. nueitų į /tmp katalogą su chdir()
 *	4. patikrintų su getcwd() ir atspausdintų koks dabar yra einamasis
 *	katalogas
 *	5. grįžtų į 2-ame žingsnyje atidarytą katalogą su fchdir() (ir
 *	patikrintų/parodytų, kad tikrai grįžo)
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define C_TMP_DIR "/tmp"

int main()
{
	char cur_dir[255];
	int fd;

	/* 1 */
	if (!getcwd(cur_dir, pathconf(".", _PC_PATH_MAX))) {
		perror("Unable to get current dir");
		exit(1);
	}
	printf("current workding dir: %s\n", cur_dir);

	/* 2 */
	fd = open(cur_dir, O_RDONLY);
	if (fd < 0) {
		perror("Unable to open current dir fd");
		exit(1);
	}
	/* 3 */
	if (chdir(C_TMP_DIR)) {
		perror("Unable to change directory");
		exit(1);
	}

	/* 4 */
	if (!getcwd(cur_dir, pathconf(".", _PC_PATH_MAX))) {
		perror("Unable to get current dir");
		exit(1);
	}
	printf("new working dir: %s\n", cur_dir);

	/* 5 */
	if (fchdir(fd)) {
		printf("unable to return\n");
		exit(1);
	}
	if (!getcwd(cur_dir, pathconf(".", _PC_PATH_MAX))) {
		perror("Unable to get current dir");
		exit(1);
	}
	printf("new working dir: %s\n", cur_dir);

	if (close(fd)) {
		printf("unable to properly close fd");
		exit(1);
	}
	return 0;
}

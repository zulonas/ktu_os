/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_midc01.c
 */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int fd;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	char *filename = "/tmp/FILE";
	char *lorem = "lorem ipsum is simply dummy text\n";

	fd = creat(filename, mode);
	if (fd == -1) {
		perror("Error occurred while creating file\n");
		exit(1);
	}


	if (write(fd, lorem, strlen(lorem)) < 0) {
		perror("Unable to write to file\n");
		exit(1);
	}

	close(fd);
	return 0;
}


/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_stat01.c
 *
 * Sukurkite programą loginas_stat01.c (loginas pakeiskite į savo loginą), kuri
 * su stat() ar fstat() gautų informaciją apie komandinės elutės parametru jai
 * nurodytą failą, katalogą ar kt. ir išvestų į ekraną stat struktūros turinį:
 *	- savininko ID
 *	- dydį
 *	- i-node numerį
 *	- leidimus
 *	- failo tipą (katalogas/failas/kanalas/soketas...)
 *	 Palyginkite savo programos ir stat komandos grąžinamus rezultatus
 *	 įvairiems failams. Turėtumėte matyti tą pačią informaciją (nebūtinai
 *	 vienodai atvaizduotą).
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	struct stat sb;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
		exit(1);
	}

	if (stat(argv[1], &sb) == -1) {
		perror("Unable to get file info");
		exit(1);
	}

	printf("Ownership:                UID=%ld\n", (long) sb.st_uid);
	printf("File size:                %lld bytes\n", (long long)
	       sb.st_size);
	printf("I-node number:            %ld\n", (long) sb.st_ino);

	printf("Preferred I/O block size: %ld bytes\n",
	       (long) sb.st_blksize);

	printf("File Permissions:         ");
	printf((S_ISDIR(sb.st_mode)) ? "d" : "-");
	printf((sb.st_mode & S_IRUSR) ? "r" : "-");
	printf((sb.st_mode & S_IWUSR) ? "w" : "-");
	printf((sb.st_mode & S_IXUSR) ? "x" : "-");
	printf((sb.st_mode & S_IRGRP) ? "r" : "-");
	printf((sb.st_mode & S_IWGRP) ? "w" : "-");
	printf((sb.st_mode & S_IXGRP) ? "x" : "-");
	printf((sb.st_mode & S_IROTH) ? "r" : "-");
	printf((sb.st_mode & S_IWOTH) ? "w" : "-");
	printf((sb.st_mode & S_IXOTH) ? "x" : "-");
	printf("\n");

	printf("File type:                ");
	switch (sb.st_mode & S_IFMT) {
	case S_IFBLK:  printf("block device\n");            break;
	case S_IFCHR:  printf("character device\n");        break;
	case S_IFDIR:  printf("directory\n");               break;
	case S_IFIFO:  printf("FIFO/pipe\n");               break;
	case S_IFLNK:  printf("symlink\n");                 break;
	case S_IFREG:  printf("regular file\n");            break;
	case S_IFSOCK: printf("socket\n");                  break;
	default:       printf("unknown?\n");                break;
	}
	printf("\n");

	return 0;
}


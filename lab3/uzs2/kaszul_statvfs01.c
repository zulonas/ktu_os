/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_statvfs01.c
 *
 * Nukopijuokite loginas_stat01.c į loginas_statvfs01.c ir papildykite, kad
 * papildomai būtų išvedama ir statvfs() arba fstatvfs() grąžinamos struktūros
 * statvfs informacija:
 *	failų sistemos bloko dydis
 *	failų sistemos ID
 *	failų sistemos dydis
 *	maksimalų failo kelio/vardo ilgis
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

int main(int argc, char *argv[])
{
	struct stat sb;
	struct statvfs svfs;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
		exit(1);
	}

	if (stat(argv[1], &sb) == -1) {
		perror("Unable to get file info");
		exit(1);
	}

	printf("Ownership:                UID=%ld\n", (long) sb.st_uid);
	printf("File size:                %lld bytes\n", (long long)
	       sb.st_size);
	printf("I-node number:            %ld\n", (long) sb.st_ino);

	printf("Preferred I/O block size: %ld bytes\n",
	       (long) sb.st_blksize);

	printf("File Permissions:         ");
	printf((S_ISDIR(sb.st_mode)) ? "d" : "-");
	printf((sb.st_mode & S_IRUSR) ? "r" : "-");
	printf((sb.st_mode & S_IWUSR) ? "w" : "-");
	printf((sb.st_mode & S_IXUSR) ? "x" : "-");
	printf((sb.st_mode & S_IRGRP) ? "r" : "-");
	printf((sb.st_mode & S_IWGRP) ? "w" : "-");
	printf((sb.st_mode & S_IXGRP) ? "x" : "-");
	printf((sb.st_mode & S_IROTH) ? "r" : "-");
	printf((sb.st_mode & S_IWOTH) ? "w" : "-");
	printf((sb.st_mode & S_IXOTH) ? "x" : "-");
	printf("\n");

	printf("File type:                ");
	switch (sb.st_mode & S_IFMT) {
	case S_IFBLK:  printf("block device\n");            break;
	case S_IFCHR:  printf("character device\n");        break;
	case S_IFDIR:  printf("directory\n");               break;
	case S_IFIFO:  printf("FIFO/pipe\n");               break;
	case S_IFLNK:  printf("symlink\n");                 break;
	case S_IFREG:  printf("regular file\n");            break;
	case S_IFSOCK: printf("socket\n");                  break;
	default:       printf("unknown?\n");                break;
	}

	if (statvfs(argv[1], &svfs) == -1) {
		perror("Unable to get file system info");
		exit(1);
	}

	printf("Filesytem block size      %ld\n", (long)svfs.f_bsize);
	printf("Filesytem ID              %ld\n", (long)svfs.f_fsid);
	printf("Filesytem size            %lld\n", (long long)svfs.f_frsize * svfs.f_blocks);
	printf("Maximum filename lenght   %ld\n", (long)svfs.f_namemax);

	return 0;
}


/* Kasparas Zulonas IFIN-8/3 kaszul */
/* Failas: kaszul_pathconf.c */
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	int name_max;
	int path_max;

	name_max = pathconf(__FILE__, _PC_NAME_MAX);
	path_max = pathconf(__FILE__, _PC_PATH_MAX);

	printf( "(C) 2020 Kasparas Zulonas, %s\n", __FILE__ );
	printf("_PC_NAME_MAX = %d\n", name_max);
	printf("_PC_PATH_MAX = %d\n", path_max);

	return 0;
}

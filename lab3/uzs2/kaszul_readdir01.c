/* Kasparas Zulonas IFIN-8/3 kaszul
 * Failas: kaszul_readdir01.c
 *
 * Sukurkite C programą loginas_readdir01.c (loginas pakeiskite į savo loginą),
 * kuri:
 *	1. atidarytų einamą katalogą su opendir() arba fdopendir();
 *	2. cikle nuskaitytų visus katalogo įrašus su readdir() ir išvestų
 *	kiekvieno įrašo i-node numerį (dirent struktūros d_ino laukas) ir failo
 *	vardą (dirent struktūros d_name laukas);
 *	3. uždarytų katalogą su closedir();
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <stdlib.h>

char *kp_test_getcwd()
{
	char *cwd;

	cwd = getcwd(NULL, pathconf(".", _PC_PATH_MAX));
	if (!cwd) {
		perror("Unable to get currnent working dir");
		exit(1);
	}

	return cwd;
}

int main(int argc, char **argv)
{
	char *cur_dir;
	DIR* cur_dir_pnt;
	struct dirent *dir_ent;

	cur_dir = kp_test_getcwd();
	/* 1 */
	cur_dir_pnt = opendir(cur_dir);
	if (!cur_dir_pnt) {
		printf("%s: \n", strerror(errno));
		exit(1);
	}

	/* 2 */
	while ((dir_ent = readdir(cur_dir_pnt)))
		printf("%ld\t%s\n", dir_ent->d_ino, dir_ent->d_name);

	/* 3 */
	if (closedir(cur_dir_pnt)) {
		printf("%s: \n", strerror(errno));
		exit(1);
	}
	free(cur_dir);
	return 0;
}

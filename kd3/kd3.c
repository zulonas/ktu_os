/*
1. Darydamas uzduotis naudojuosi tik LD1, LD2, LD3 aprasais,
   sistemos pagalbos sistema (man ir pan.), savo darytomis LD uzduotimis
2. Užduoti darau savarankiškai be treciuju asmenu pagalbos
3. Uzduoti koreguoju naudodamasis vienu kompiuteriu

* Kasparas Zulonas SUTINKU
*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define C_BUF_SIZE 10000

int file_open(const char *name)
{
	int dskr;

	dskr = open(name, O_RDWR | O_CREAT, 0640);
	if (dskr == -1) {
		perror(name);
		exit(1);
	}

	return dskr;
}

void write_in_file(int d, int offset, char *buf)
{
	lseek(d, offset - 1, SEEK_SET);
	write(d, buf, 1);
}

int file_close(int fd)
{
	int rv;

	rv = close(fd);
	if (rv)
		perror("close() failed");
	else
		printf("closed\n");

	return rv;
}

void replace_in_file(int fd, int offset)
{
	lseek(fd, offset, SEEK_SET);
	if (write(fd, "S", 1) == -1) {
		perror("nepavyko įrašyti į failą\n");
		return;
	}

	printf("Baitas %d pakeistas i simboli S\n", offset);
}

int get_middle_offset(int fd)
{
	struct stat st;


	if (fstat(fd, &st) == -1) {
		perror("unable to get middle offset\n");
	}

	printf("Dydis: %ld, vidurys:\n", st.st_size);

	if (st.st_size % 2 == 0)
		return (int) (st.st_size / 2) - 1;

	return (int) (st.st_size / 2);
}

int main(int argc, char *argv[])
{
	int fd;

	printf("(C) 2020 Kasparas Zulonas, kd3.c\n");
	if (!(argc == 3 || argc == 2)) {
		fprintf(stderr, "Naudojimas: %s failas1 [failas2]\n", argv[0]);
		exit(1);
	}

	if (argc == 2) {
		printf("Nurodytas 1 argumentas: %s\n", argv[1]);
	} else if (argc == 3) {
		printf("Nurodytas 2 argumentai: %s %s\n", argv[1], argv[2]);
	}

	if (!access(argv[1], F_OK)) {
		fd = file_open(argv[1]);
		printf("desk = %d \n", fd);
		/*printf("offset %d\n",  get_middle_offset(fd));*/
		replace_in_file(fd, get_middle_offset(fd));
	} else {
		/* if file exits */
		fd = file_open(argv[1]);
		printf("dskr = %d \n", fd);
		write_in_file(fd, 1024 * 4, "\0");
	}

	file_close(fd);
	return 0;
}
